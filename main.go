package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"time"
)

type emailAddress struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

type emailParams struct {
	Title   string `json:"title"`
	TestURL string `json:"test_url"`
}

type emailTemplate struct {
	TemplateID int            `json:"templateId"`
	Recipient  []emailAddress `json:"to"`
	ReplyTo    emailAddress   `json:"replyTo"`
	Sender     emailAddress   `json:"sender"`
	Params     emailParams    `json:"params"`
}

func main() {
	URL := "https://api.sendinblue.com/v3"
	APIKey := "API KEY GOES HERE"

	recipient := emailAddress{
		Name:  "Steve Newell",
		Email: "newellista@gmail.com",
	}

	var recipients []emailAddress

	recipients = append(recipients, recipient)

	sender := emailAddress{
		Name:  "Rucksack Travel",
		Email: "travel@rucksacktravel.com",
	}

	params := emailParams{
		Title:   "Test Title",
		TestURL: "<a href=\"www.example.com\">Example URL</a>",
	}

	template := emailTemplate{
		TemplateID: 10,
		Recipient:  recipients,
		Sender:     sender,
		ReplyTo:    sender,
		Params:     params,
	}

	jsonParams, err := json.Marshal(template)

	if err != nil {
		log.Fatal("Unable to marshal params")
	}

	client := &http.Client{Timeout: 15 * time.Second}

	url := fmt.Sprintf("%v/smtp/email", URL)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonParams))

	if err != nil {
		log.Print(err)
		return
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("api-key", APIKey)

	requestDump, err := httputil.DumpRequest(req, true)

	if err != nil {
		log.Print(err)
	}

	log.Print(string(requestDump))

	response, err := client.Do(req)

	if err != nil {
		log.Print(err)
		return
	}

	if response.StatusCode == 200 {
		return
	}

	defer response.Body.Close()

	responseData, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Printf("Unable to read response body: %v", err)
		return
	}

	var r interface{}

	err = json.Unmarshal(responseData, &r)

	if err != nil {
		log.Printf("Unable to unmarshal response: %v", err)
		return
	}

	log.Printf("Send Email returned: Status: %v, Details: %v", response.Status, r)
}
